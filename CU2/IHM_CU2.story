<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels id="ha91Z1C4C6IxKzy9Y_NF5XiP98Y=" x="75" y="75">
    <screen href="ZoneClient.screen#/"/>
  </panels>
  <panels id="ToyjFLbF7Oz7GclrcnMExMwaY_o=" x="375" y="75">
    <screen href="DetailZoneClientEdition.screen#/"/>
  </panels>
  <panels id="e2tKnB5y8lC19Ur8qKludkshpJk=" x="675" y="75">
    <screen href="DetailPersonne.screen#/"/>
  </panels>
  <panels id="h5gh3iknQqL_4EhiIw18goKnUXY=" x="975" y="75">
    <screen href="AjoutPersonne.screen#/"/>
  </panels>
  <panels id="-IhKQfj4Q_QOXe53WKYyxQRaNJw=" x="75" y="375">
    <screen href="CreerPersonne.screen#/"/>
  </panels>
  <panels id="rYY4En_wOwFfb3bOtA5blvqreoM=" x="375" y="375">
    <screen href="DetailZoneClientAjoutPersonne.screen#/"/>
  </panels>
  <panels id="zwlzPnXrfStpm2w-erFoOWC6Msc=" x="675" y="375">
    <screen href="DetailZoneClientModificationPersonne.screen#/"/>
  </panels>
  <panels id="zfB9Z50k9HINk2mt6S8P7snNopY=" x="975" y="375">
    <screen href="DetailZoneClientSuppressionPersonne.screen#/"/>
  </panels>
  <panels id="0lIoPusLpZ0x4DRdTlgeGIShBDY=" x="75" y="675">
    <screen href="DetailPersonneReadOnly.screen#/"/>
  </panels>
</story:Storyboard>
