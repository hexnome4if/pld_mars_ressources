var story = {
 "pages": [
  {
   "image": "ZoneRelationClient-VarianteDoubleBouton.png",
   "image2x": "ZoneRelationClient-VarianteDoubleBouton@2x.png",
   "width": 927,
   "links": [{
    "rect": [
     386,
     238,
     562,
     327
    ],
    "page": 1
   }],
   "title": "ZoneRelationClient-VarianteDoubleBouton",
   "height": 343
  },
  {
   "image": "ListeHistorique.png",
   "image2x": "ListeHistorique@2x.png",
   "width": 1023,
   "links": [
    {
     "rect": [
      782,
      403,
      868,
      431
     ],
     "page": 5
    },
    {
     "rect": [
      782,
      332,
      868,
      360
     ],
     "page": 2
    },
    {
     "rect": [
      782,
      182,
      868,
      210
     ],
     "page": 3
    },
    {
     "rect": [
      878,
      403,
      1007,
      431
     ],
     "page": 8
    },
    {
     "rect": [
      160,
      454,
      465,
      482
     ],
     "page": 7
    },
    {
     "rect": [
      478,
      454,
      783,
      482
     ],
     "page": 6
    }
   ],
   "title": "ListeHistorique",
   "height": 519
  },
  {
   "image": "InfosContactDetaillees_ExAnnulé.png",
   "image2x": "InfosContactDetaillees_ExAnnulé@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      574,
      128,
      863,
      156
     ],
     "page": 8
    },
    {
     "rect": [
      318,
      492,
      515,
      520
     ],
     "page": 1
    },
    {
     "rect": [
      110,
      492,
      307,
      520
     ],
     "page": 1
    },
    {
     "rect": [
      784,
      231,
      870,
      259
     ],
     "page": 15
    },
    {
     "rect": [
      784,
      412,
      870,
      440
     ],
     "page": 14
    },
    {
     "rect": [
      617,
      316,
      808,
      344
     ],
     "page": 13
    },
    {
     "rect": [
      616,
      499,
      821,
      527
     ],
     "page": 9
    }
   ],
   "title": "InfosContactDetaillees_ExAnnulé",
   "height": 537
  },
  {
   "image": "InfosContactDetaillees_ExPlanifie.png",
   "image2x": "InfosContactDetaillees_ExPlanifie@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      149,
      324,
      278,
      352
     ],
     "page": 5
    },
    {
     "rect": [
      295,
      324,
      438,
      352
     ],
     "page": 2
    },
    {
     "rect": [
      585,
      136,
      874,
      164
     ],
     "page": 8
    },
    {
     "rect": [
      318,
      500,
      515,
      528
     ],
     "page": 1
    },
    {
     "rect": [
      110,
      500,
      307,
      528
     ],
     "page": 1
    },
    {
     "rect": [
      784,
      239,
      870,
      267
     ],
     "page": 15
    },
    {
     "rect": [
      784,
      420,
      870,
      448
     ],
     "page": 14
    },
    {
     "rect": [
      617,
      324,
      808,
      352
     ],
     "page": 13
    },
    {
     "rect": [
      616,
      507,
      821,
      535
     ],
     "page": 9
    }
   ],
   "title": "InfosContactDetaillees_ExPlanifie",
   "height": 545
  },
  {
   "image": "InfosContactDetaillees_ExPlanifie_entretien.png",
   "image2x": "InfosContactDetaillees_ExPlanifie_entretien@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      149,
      316,
      278,
      344
     ],
     "page": 5
    },
    {
     "rect": [
      295,
      316,
      438,
      344
     ],
     "page": 2
    },
    {
     "rect": [
      585,
      136,
      874,
      164
     ],
     "page": 8
    },
    {
     "rect": [
      318,
      500,
      515,
      528
     ],
     "page": 1
    },
    {
     "rect": [
      110,
      500,
      307,
      528
     ],
     "page": 1
    },
    {
     "rect": [
      784,
      239,
      870,
      267
     ],
     "page": 15
    },
    {
     "rect": [
      784,
      420,
      870,
      448
     ],
     "page": 14
    },
    {
     "rect": [
      617,
      324,
      808,
      352
     ],
     "page": 13
    },
    {
     "rect": [
      616,
      507,
      821,
      535
     ],
     "page": 9
    }
   ],
   "title": "InfosContactDetaillees_ExPlanifie_entretien",
   "height": 545
  },
  {
   "image": "InfosContactDetaillees_ExRealise.png",
   "image2x": "InfosContactDetaillees_ExRealise@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      574,
      131,
      863,
      159
     ],
     "page": 8
    },
    {
     "rect": [
      318,
      495,
      515,
      523
     ],
     "page": 1
    },
    {
     "rect": [
      110,
      495,
      307,
      523
     ],
     "page": 1
    },
    {
     "rect": [
      617,
      319,
      808,
      347
     ],
     "page": 13
    },
    {
     "rect": [
      616,
      502,
      821,
      530
     ],
     "page": 9
    },
    {
     "rect": [
      784,
      234,
      870,
      262
     ],
     "page": 15
    },
    {
     "rect": [
      784,
      415,
      870,
      443
     ],
     "page": 14
    }
   ],
   "title": "InfosContactDetaillees_ExRealise",
   "height": 540
  },
  {
   "image": "AjoutContactCommercial.png",
   "image2x": "AjoutContactCommercial@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      523,
      414,
      720,
      442
     ],
     "page": 3
    },
    {
     "rect": [
      78,
      414,
      275,
      442
     ],
     "page": 1
    }
   ],
   "title": "AjoutContactCommercial",
   "height": 463
  },
  {
   "image": "AjoutContactSpontane.png",
   "image2x": "AjoutContactSpontane@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      506,
      390,
      703,
      418
     ],
     "page": 5
    },
    {
     "rect": [
      102,
      390,
      299,
      418
     ],
     "page": 1
    }
   ],
   "title": "AjoutContactSpontane",
   "height": 439
  },
  {
   "image": "AjoutEntretien.png",
   "image2x": "AjoutEntretien@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      506,
      351,
      703,
      379
     ],
     "page": 4
    },
    {
     "rect": [
      102,
      351,
      299,
      379
     ],
     "page": 1
    }
   ],
   "title": "AjoutEntretien",
   "height": 392
  },
  {
   "image": "AjoutProposition_Contrat.png",
   "image2x": "AjoutProposition_Contrat@2x.png",
   "width": 894,
   "links": [
    {
     "rect": [
      464,
      408,
      661,
      443
     ],
     "action": "back"
    },
    {
     "rect": [
      247,
      408,
      444,
      443
     ],
     "action": "back"
    },
    {
     "rect": [
      259,
      312,
      410,
      347
     ],
     "page": 10
    }
   ],
   "title": "AjoutProposition_Contrat",
   "height": 488
  },
  {
   "image": "AjoutProposition_ContratAvecDetails.png",
   "image2x": "AjoutProposition_ContratAvecDetails@2x.png",
   "width": 894,
   "links": [
    {
     "rect": [
      464,
      408,
      661,
      443
     ],
     "action": "back"
    },
    {
     "rect": [
      247,
      408,
      444,
      443
     ],
     "action": "back"
    },
    {
     "rect": [
      259,
      312,
      410,
      347
     ],
     "page": 10
    },
    {
     "rect": [
      424,
      450,
      492,
      478
     ],
     "page": 9
    }
   ],
   "title": "AjoutProposition_ContratAvecDetails",
   "height": 488
  },
  {
   "image": "AjouterContratAvecDetails.png",
   "image2x": "AjouterContratAvecDetails@2x.png",
   "width": 922,
   "links": [
    {
     "rect": [
      144,
      533,
      341,
      559
     ],
     "action": "back"
    },
    {
     "rect": [
      513,
      531,
      800,
      561
     ],
     "page": 9
    },
    {
     "rect": [
      422,
      486,
      486,
      512
     ],
     "page": 12
    }
   ],
   "title": "AjouterContratAvecDetails",
   "height": 590
  },
  {
   "image": "AjouterContrat.png",
   "image2x": "AjouterContrat@2x.png",
   "width": 922,
   "links": [
    {
     "rect": [
      144,
      533,
      341,
      559
     ],
     "action": "back"
    },
    {
     "rect": [
      513,
      531,
      800,
      561
     ],
     "page": 9
    },
    {
     "rect": [
      358,
      533,
      503,
      559
     ],
     "page": 11
    }
   ],
   "title": "AjouterContrat",
   "height": 590
  },
  {
   "image": "AjoutProposition_Offre.png",
   "image2x": "AjoutProposition_Offre@2x.png",
   "width": 894,
   "links": [
    {
     "rect": [
      464,
      344,
      661,
      372
     ],
     "action": "back"
    },
    {
     "rect": [
      247,
      344,
      444,
      372
     ],
     "action": "back"
    }
   ],
   "title": "AjoutProposition_Offre",
   "height": 385
  },
  {
   "image": "DetailsProposition_Contrat.png",
   "image2x": "DetailsProposition_Contrat@2x.png",
   "width": 894,
   "links": [
    {
     "rect": [
      464,
      344,
      661,
      372
     ],
     "action": "back"
    },
    {
     "rect": [
      247,
      344,
      444,
      372
     ],
     "action": "back"
    }
   ],
   "title": "DetailsProposition_Contrat",
   "height": 385
  },
  {
   "image": "DetailsProposition_Offre.png",
   "image2x": "DetailsProposition_Offre@2x.png",
   "width": 894,
   "links": [
    {
     "rect": [
      464,
      344,
      661,
      372
     ],
     "action": "back"
    },
    {
     "rect": [
      247,
      344,
      444,
      372
     ],
     "action": "back"
    }
   ],
   "title": "DetailsProposition_Offre",
   "height": 385
  }
 ],
 "resolutions": [2],
 "title": "TotalCU3",
 "highlightLinks": true
}