var story = {
 "pages": [
  {
   "image": "ZoneRelationClient-VarianteDoubleBouton.png",
   "image2x": "ZoneRelationClient-VarianteDoubleBouton@2x.png",
   "width": 927,
   "links": [{
    "rect": [
     386,
     238,
     562,
     327
    ],
    "page": 1
   }],
   "title": "ZoneRelationClient-VarianteDoubleBouton",
   "height": 343
  },
  {
   "image": "ListeHistorique.png",
   "image2x": "ListeHistorique@2x.png",
   "width": 1023,
   "links": [
    {
     "rect": [
      782,
      182,
      868,
      210
     ],
     "page": 5
    },
    {
     "rect": [
      782,
      259,
      868,
      287
     ],
     "page": 2
    },
    {
     "rect": [
      782,
      411,
      868,
      439
     ],
     "page": 3
    },
    {
     "rect": [
      878,
      182,
      1007,
      210
     ],
     "page": 8
    },
    {
     "rect": [
      160,
      454,
      465,
      482
     ],
     "page": 7
    },
    {
     "rect": [
      478,
      454,
      783,
      482
     ],
     "page": 6
    }
   ],
   "title": "ListeHistorique",
   "height": 519
  },
  {
   "image": "InfosContactDetaillees_ExAnnulé.png",
   "image2x": "InfosContactDetaillees_ExAnnulé@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      574,
      110,
      863,
      138
     ],
     "page": 8
    },
    {
     "rect": [
      318,
      474,
      515,
      502
     ],
     "page": 1
    },
    {
     "rect": [
      110,
      474,
      307,
      502
     ],
     "page": 1
    },
    {
     "rect": [
      617,
      298,
      808,
      326
     ],
     "page": 12
    },
    {
     "rect": [
      616,
      481,
      821,
      509
     ],
     "page": 11
    },
    {
     "rect": [
      784,
      213,
      870,
      241
     ],
     "page": 12
    },
    {
     "rect": [
      784,
      394,
      870,
      422
     ],
     "page": 11
    }
   ],
   "title": "InfosContactDetaillees_ExAnnulé",
   "height": 519
  },
  {
   "image": "InfosContactDetaillees_ExPlanifie.png",
   "image2x": "InfosContactDetaillees_ExPlanifie@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      149,
      298,
      278,
      326
     ],
     "page": 5
    },
    {
     "rect": [
      295,
      298,
      438,
      326
     ],
     "page": 2
    },
    {
     "rect": [
      585,
      110,
      874,
      138
     ],
     "page": 8
    },
    {
     "rect": [
      318,
      474,
      515,
      502
     ],
     "page": 1
    },
    {
     "rect": [
      110,
      474,
      307,
      502
     ],
     "page": 1
    },
    {
     "rect": [
      617,
      298,
      808,
      326
     ],
     "page": 12
    },
    {
     "rect": [
      616,
      481,
      821,
      509
     ],
     "page": 11
    },
    {
     "rect": [
      784,
      213,
      870,
      241
     ],
     "page": 12
    },
    {
     "rect": [
      784,
      394,
      870,
      422
     ],
     "page": 11
    }
   ],
   "title": "InfosContactDetaillees_ExPlanifie",
   "height": 519
  },
  {
   "image": "InfosContactDetaillees_ExPlanifie_entretien.png",
   "image2x": "InfosContactDetaillees_ExPlanifie_entretien@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      149,
      290,
      278,
      318
     ],
     "page": 5
    },
    {
     "rect": [
      295,
      290,
      438,
      318
     ],
     "page": 2
    },
    {
     "rect": [
      585,
      110,
      874,
      138
     ],
     "page": 8
    },
    {
     "rect": [
      318,
      474,
      515,
      502
     ],
     "page": 1
    },
    {
     "rect": [
      110,
      474,
      307,
      502
     ],
     "page": 1
    },
    {
     "rect": [
      617,
      298,
      808,
      326
     ],
     "page": 12
    },
    {
     "rect": [
      616,
      481,
      821,
      509
     ],
     "page": 11
    },
    {
     "rect": [
      784,
      213,
      870,
      241
     ],
     "page": 12
    },
    {
     "rect": [
      784,
      394,
      870,
      422
     ],
     "page": 11
    }
   ],
   "title": "InfosContactDetaillees_ExPlanifie_entretien",
   "height": 519
  },
  {
   "image": "InfosContactDetaillees_ExRealise.png",
   "image2x": "InfosContactDetaillees_ExRealise@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      574,
      110,
      863,
      138
     ],
     "page": 8
    },
    {
     "rect": [
      318,
      474,
      515,
      502
     ],
     "page": 1
    },
    {
     "rect": [
      110,
      474,
      307,
      502
     ],
     "page": 1
    },
    {
     "rect": [
      617,
      298,
      808,
      326
     ],
     "page": 12
    },
    {
     "rect": [
      616,
      481,
      821,
      509
     ],
     "page": 11
    },
    {
     "rect": [
      784,
      213,
      870,
      241
     ],
     "page": 12
    },
    {
     "rect": [
      784,
      394,
      870,
      422
     ],
     "page": 11
    }
   ],
   "title": "InfosContactDetaillees_ExRealise",
   "height": 519
  },
  {
   "image": "AjoutContactCommercial.png",
   "image2x": "AjoutContactCommercial@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      523,
      414,
      720,
      442
     ],
     "page": 3
    },
    {
     "rect": [
      78,
      414,
      275,
      442
     ],
     "page": 1
    }
   ],
   "title": "AjoutContactCommercial",
   "height": 463
  },
  {
   "image": "AjoutContactSpontane.png",
   "image2x": "AjoutContactSpontane@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      506,
      390,
      703,
      418
     ],
     "page": 3
    },
    {
     "rect": [
      102,
      390,
      299,
      418
     ],
     "page": 1
    }
   ],
   "title": "AjoutContactSpontane",
   "height": 439
  },
  {
   "image": "AjoutEntretien.png",
   "image2x": "AjoutEntretien@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      506,
      350,
      703,
      378
     ],
     "page": 4
    },
    {
     "rect": [
      102,
      350,
      299,
      378
     ],
     "page": 1
    }
   ],
   "title": "AjoutEntretien",
   "height": 391
  },
  {
   "image": "AjoutProposition_Contrat.png",
   "image2x": "AjoutProposition_Contrat@2x.png",
   "width": 894,
   "links": [
    {
     "rect": [
      464,
      294,
      661,
      322
     ],
     "action": "back"
    },
    {
     "rect": [
      247,
      294,
      444,
      322
     ],
     "action": "back"
    }
   ],
   "title": "AjoutProposition_Contrat",
   "height": 335
  },
  {
   "image": "AjoutProposition_Offre.png",
   "image2x": "AjoutProposition_Offre@2x.png",
   "width": 894,
   "links": [
    {
     "rect": [
      464,
      294,
      661,
      322
     ],
     "action": "back"
    },
    {
     "rect": [
      247,
      294,
      444,
      322
     ],
     "action": "back"
    }
   ],
   "title": "AjoutProposition_Offre",
   "height": 335
  },
  {
   "image": "DetailsProposition_Contrat.png",
   "image2x": "DetailsProposition_Contrat@2x.png",
   "width": 894,
   "links": [
    {
     "rect": [
      464,
      294,
      661,
      322
     ],
     "action": "back"
    },
    {
     "rect": [
      247,
      294,
      444,
      322
     ],
     "action": "back"
    }
   ],
   "title": "DetailsProposition_Contrat",
   "height": 335
  },
  {
   "image": "DetailsProposition_Offre.png",
   "image2x": "DetailsProposition_Offre@2x.png",
   "width": 894,
   "links": [
    {
     "rect": [
      464,
      294,
      661,
      322
     ],
     "action": "back"
    },
    {
     "rect": [
      247,
      294,
      444,
      322
     ],
     "action": "back"
    }
   ],
   "title": "DetailsProposition_Offre",
   "height": 335
  }
 ],
 "resolutions": [2],
 "title": "TotalCU3",
 "highlightLinks": true
}