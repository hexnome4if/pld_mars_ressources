var story = {
 "pages": [
  {
   "image": "ZoneRelationClient.png",
   "image2x": "ZoneRelationClient@2x.png",
   "width": 850,
   "links": [
    {
     "rect": [
      264,
      174,
      418,
      257
     ],
     "page": 2
    },
    {
     "rect": [
      30,
      174,
      247,
      202
     ],
     "page": 7
    }
   ],
   "title": "ZoneRelationClient",
   "height": 287
  },
  {
   "image": "ZoneRelationClient-VarianteDoubleBouton.png",
   "image2x": "ZoneRelationClient-VarianteDoubleBouton@2x.png",
   "width": 850,
   "links": [{
    "rect": [
     334,
     187,
     488,
     270
    ],
    "page": 2
   }],
   "title": "ZoneRelationClient-VarianteDoubleBouton",
   "height": 287
  },
  {
   "image": "ListeHistorique.png",
   "image2x": "ListeHistorique@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      554,
      454,
      771,
      482
     ],
     "page": 7
    },
    {
     "rect": [
      782,
      182,
      868,
      210
     ],
     "page": 6
    },
    {
     "rect": [
      782,
      259,
      868,
      287
     ],
     "page": 4
    },
    {
     "rect": [
      782,
      411,
      868,
      439
     ],
     "page": 5
    }
   ],
   "title": "ListeHistorique",
   "height": 519
  },
  {
   "image": "ListeHistorique-VarianteDoubleBouton.png",
   "image2x": "ListeHistorique-VarianteDoubleBouton@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      782,
      182,
      868,
      210
     ],
     "page": 6
    },
    {
     "rect": [
      782,
      259,
      868,
      287
     ],
     "page": 4
    },
    {
     "rect": [
      782,
      411,
      868,
      439
     ],
     "page": 5
    }
   ],
   "title": "ListeHistorique-VarianteDoubleBouton",
   "height": 519
  },
  {
   "image": "InfosContactDetaillees_ExAnnulé.png",
   "image2x": "InfosContactDetaillees_ExAnnulé@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      574,
      53,
      863,
      81
     ],
     "page": 8
    },
    {
     "rect": [
      318,
      474,
      515,
      502
     ],
     "page": 2
    },
    {
     "rect": [
      110,
      474,
      307,
      502
     ],
     "page": 2
    },
    {
     "rect": [
      617,
      298,
      820,
      326
     ],
     "page": 12
    },
    {
     "rect": [
      616,
      481,
      821,
      509
     ],
     "page": 11
    },
    {
     "rect": [
      784,
      213,
      870,
      241
     ],
     "page": 9
    },
    {
     "rect": [
      784,
      394,
      870,
      422
     ],
     "page": 9
    }
   ],
   "title": "InfosContactDetaillees_ExAnnulé",
   "height": 519
  },
  {
   "image": "InfosContactDetaillees_ExPlanifie.png",
   "image2x": "InfosContactDetaillees_ExPlanifie@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      574,
      94,
      703,
      122
     ],
     "page": 6
    },
    {
     "rect": [
      720,
      94,
      863,
      122
     ],
     "page": 4
    },
    {
     "rect": [
      574,
      53,
      863,
      81
     ],
     "page": 8
    },
    {
     "rect": [
      318,
      474,
      515,
      502
     ],
     "page": 2
    },
    {
     "rect": [
      110,
      474,
      307,
      502
     ],
     "page": 2
    },
    {
     "rect": [
      617,
      298,
      820,
      326
     ],
     "page": 12
    },
    {
     "rect": [
      616,
      481,
      821,
      509
     ],
     "page": 11
    },
    {
     "rect": [
      784,
      213,
      870,
      241
     ],
     "page": 9
    },
    {
     "rect": [
      784,
      394,
      870,
      422
     ],
     "page": 9
    }
   ],
   "title": "InfosContactDetaillees_ExPlanifie",
   "height": 519
  },
  {
   "image": "InfosContactDetaillees_ExRealise.png",
   "image2x": "InfosContactDetaillees_ExRealise@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      574,
      53,
      863,
      81
     ],
     "page": 8
    },
    {
     "rect": [
      318,
      474,
      515,
      502
     ],
     "page": 2
    },
    {
     "rect": [
      110,
      474,
      307,
      502
     ],
     "page": 2
    },
    {
     "rect": [
      617,
      298,
      820,
      326
     ],
     "page": 12
    },
    {
     "rect": [
      616,
      481,
      821,
      509
     ],
     "page": 11
    },
    {
     "rect": [
      784,
      213,
      870,
      241
     ],
     "page": 9
    },
    {
     "rect": [
      784,
      394,
      870,
      422
     ],
     "page": 9
    }
   ],
   "title": "InfosContactDetaillees_ExRealise",
   "height": 519
  },
  {
   "image": "AjoutContact.png",
   "image2x": "AjoutContact@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      506,
      481,
      703,
      509
     ],
     "page": 5
    },
    {
     "rect": [
      102,
      481,
      299,
      509
     ],
     "page": 2
    }
   ],
   "title": "AjoutContact",
   "height": 519
  },
  {
   "image": "AjoutEntretien.png",
   "image2x": "AjoutEntretien@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      506,
      422,
      703,
      450
     ],
     "page": 5
    },
    {
     "rect": [
      102,
      422,
      299,
      450
     ],
     "page": 2
    }
   ],
   "title": "AjoutEntretien",
   "height": 463
  },
  {
   "image": "InfoPropositionDetaillees.png",
   "image2x": "InfoPropositionDetaillees@2x.png",
   "width": 908,
   "links": [
    {
     "rect": [
      660,
      286,
      863,
      314
     ],
     "page": 12
    },
    {
     "rect": [
      658,
      459,
      863,
      487
     ],
     "page": 11
    },
    {
     "rect": [
      253,
      550,
      450,
      578
     ],
     "action": "back"
    },
    {
     "rect": [
      36,
      550,
      233,
      578
     ],
     "action": "back"
    }
   ],
   "title": "InfoPropositionDetaillees",
   "height": 599
  },
  {
   "image": "AjoutProposition.png",
   "image2x": "AjoutProposition@2x.png",
   "width": 894,
   "links": [
    {
     "rect": [
      461,
      406,
      658,
      434
     ],
     "action": "back"
    },
    {
     "rect": [
      244,
      406,
      441,
      434
     ],
     "action": "back"
    }
   ],
   "title": "AjoutProposition",
   "height": 455
  },
  {
   "image": "AjoutProposition_Contrat.png",
   "image2x": "AjoutProposition_Contrat@2x.png",
   "width": 894,
   "links": [
    {
     "rect": [
      464,
      294,
      661,
      322
     ],
     "action": "back"
    },
    {
     "rect": [
      247,
      294,
      444,
      322
     ],
     "action": "back"
    }
   ],
   "title": "AjoutProposition_Contrat",
   "height": 335
  },
  {
   "image": "AjoutProposition_Produit.png",
   "image2x": "AjoutProposition_Produit@2x.png",
   "width": 894,
   "links": [
    {
     "rect": [
      464,
      318,
      661,
      346
     ],
     "action": "back"
    },
    {
     "rect": [
      247,
      318,
      444,
      346
     ],
     "action": "back"
    }
   ],
   "title": "AjoutProposition_Produit",
   "height": 359
  }
 ],
 "resolutions": [2],
 "title": "TotalCU3",
 "highlightLinks": true
}