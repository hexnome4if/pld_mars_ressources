<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels id="3dK4W-CHg3BTZfJ7qhfSVJVXoCI=" x="375" y="975">
    <screen href="ZoneRelationVue360/ZoneRelationClient-VarianteDoubleBouton.screen#/"/>
  </panels>
  <panels id="1nL8466tReLFv8n1KjQpxKYiBxg=" x="75" y="975">
    <screen href="HistoriqueContacts/ListeHistorique.screen#/"/>
  </panels>
  <panels id="vZyPRprPBTCtuDZKaEmD5L8dwV8=" x="75" y="675">
    <screen href="DetailsContact/InfosContactDetaillees_ExAnnulé.screen#/"/>
  </panels>
  <panels id="6NlFoYyCWB7j03bFHa1kQI9K4fo=" x="375" y="675">
    <screen href="DetailsContact/InfosContactDetaillees_ExPlanifie.screen#/"/>
  </panels>
  <panels id="isPqlkLXPPaSvwMuRjP9Ln11Ow4=" x="675" y="675">
    <screen href="DetailsContact/InfosContactDetaillees_ExPlanifie_entretien.screen#/"/>
  </panels>
  <panels id="nyD6XRnpsGbTQcOX0IEDFCQDpdE=" x="975" y="675">
    <screen href="DetailsContact/InfosContactDetaillees_ExRealise.screen#/"/>
  </panels>
  <panels id="jeOYNzSQ967WfecYig9RjJGvBfw=" x="75" y="75">
    <screen href="AjoutContact/AjoutContactCommercial.screen#/"/>
  </panels>
  <panels id="9AioLDM9zRbmYsLyTvtPvi0UYc4=" x="375" y="75">
    <screen href="AjoutContact/AjoutContactSpontane.screen#/"/>
  </panels>
  <panels id="oV0OsS1tn-zTtblTvKPtw17If60=" x="675" y="75">
    <screen href="AjoutContact/AjoutEntretien.screen#/"/>
  </panels>
  <panels id="XtE5HgV6UU8eu_5aOIPkomxPNow=" x="975" y="75">
    <screen href="AjoutProposition/AjoutProposition_Contrat.screen#/"/>
  </panels>
  <panels id="9WpDv_SwHwh7KdT5cTdNIPqf-Tc=" x="675" y="975">
    <screen href="AjoutProposition/AjoutProposition_ContratAvecDetails.screen#/"/>
  </panels>
  <panels id="vlUiA71JGNw8TNkAnaitJ2qnWFc=" x="675" y="1275">
    <screen href="AjoutProposition/AjouterContratAvecDetails.screen#/"/>
  </panels>
  <panels id="ZmhzmZjWW9Xv2EZ0gsSJJRcFv0M=" x="375" y="1275">
    <screen href="AjoutProposition/AjouterContrat.screen#/"/>
  </panels>
  <panels id="vfatKo4Ta_titrHCKhAP_95lrKM=" x="75" y="375">
    <screen href="AjoutProposition/AjoutProposition_Offre.screen#/"/>
  </panels>
  <panels id="DPr56vsqEcSBn2PjttWz1INqoxA=" x="375" y="375">
    <screen href="AjoutProposition/DetailsProposition_Contrat.screen#/"/>
  </panels>
  <panels id="7-0FgNdK5LAy7Ll4XubbfdiFzc8=" x="675" y="375">
    <screen href="AjoutProposition/DetailsProposition_Offre.screen#/"/>
  </panels>
</story:Storyboard>
