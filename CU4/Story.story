<?xml version="1.0" encoding="UTF-8"?>
<story:Storyboard xmlns:story="http://wireframesketcher.com/1.0/model/story">
  <panels id="iKmNm-uKpHVXqdM48xO61C2xYl4=" x="675" y="75">
    <screen href="ConsulterListeContrats.screen#/"/>
  </panels>
  <panels id="XrtLEC-DDlQW-0MloY5GOGknYWY=" x="375" y="75">
    <screen href="ConsulterContratOptionSouscrite.screen#/"/>
  </panels>
  <panels id="_U2PocgIuV9jkSke_uCJMpwP3d4=" x="375" y="375">
    <screen href="ONLYResiliationConfirmation.screen#/"/>
  </panels>
  <panels id="2cokXrWZNObLlCq1hXxCC4EFAeA=" x="975" y="75">
    <screen href="ONLYDetailsContrat.screen#/"/>
  </panels>
  <panels id="8obNVHJxH6bMsT4L0PhC1NBq7UE=" x="75" y="675">
    <screen href="DetailsOption.screen#/"/>
  </panels>
  <panels id="7MwUdZQmJjD9JUPlQZWRT_SBgU8=" x="75" y="375">
    <screen href="AjouterContrat.screen#/"/>
  </panels>
  <panels id="yLioL3z8LKAeYY8NQpj4aCMOPUM=" x="675" y="375">
    <screen href="ONLYSouscriptionConfirmation.screen#/"/>
  </panels>
</story:Storyboard>
